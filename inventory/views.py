from django.shortcuts import render, get_object_or_404, redirect
from django.http import Http404


def index(request):
    return render(request, 'inventory/index.html')


